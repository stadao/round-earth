#!/usr/bin/python3

# This is a sketch, for now!

from typing import Optional
import numpy as np
from numpy.linalg import det, norm
from scipy.optimize import root


def sq_dist(p1: np.ndarray, p2: np.ndarray) -> np.float64:
    '''returns the square of the euclidean distance between p1 and p2'''

    if p1.shape != p2.shape:
        print("incompatible points.")
        return 0.0

    return norm(p1-p2)**2


def find_center(points: np.ndarray,
                eps: np.float64=10**-9) -> Optional[np.ndarray]:
    '''given an array of four points, find the center of the sphere
    that goes through all of them. The parameter eps is the tolerance
    before we declare that the points passed are coplanar and
    therefore no center can be found.

    '''

    if points.shape != (4,3):
        print(f"expected points to have shape (4,3), got {points.shape} instead.")
        return None

    # we need to check that the points are not coplanar.
    u = points[1] - points[0]
    v = points[2] - points[0]
    w = points[3] - points[0]
    volume = np.abs(det(np.array([u, v, w])))

    if volume < eps:
        print(f"coplanar points, no center can be found.")
        return None

    def f(x: np.ndarray, *args):

        if x.shape != (3,):
            return -1 # ?
        
        return [sq_dist(x, points[0]) - sq_dist(x, points[1]),
                sq_dist(x, points[0]) - sq_dist(x, points[2]),
                sq_dist(x, points[0]) - sq_dist(x, points[3])]

    return root(f, (points[0]+points[1]+points[2]+points[3])/4).x


def find_radius(points: np.ndarray, eps: np.float64=10**-9) -> Optional[np.float64]:
    ''' returns the radius of the sphere that passes to the given points. '''

    if points.shape != (4,3):
        print(f"expected points to have shape (4,3), got {points.shape} instead.")
        return None

    center = find_center(points, eps)
    if center is None:
        return None

    return norm(center-points[0])


def fit_measure(points: np.ndarray) -> Optional[np.float64]:
    '''given an array points of points in the 3d space, we determine
    the sphere that passes through the first four points and check how
    well the other points adjust to this sphere, by calculating the
    average of the distance of each of these points to the center of
    the sphere, divided by its radius.
    '''

    if points.shape[0] < 4:
        print(f"fit_measure: expected at least 4 points, got {points.shape[0]}.")
        return None

    center = find_center(points[0:4])
    radius = find_radius(points[0:4])

    if center is None or radius is None:
        return None

    remaining_points = points[4:]

    if len(remaining_points) == 0:
        return 0
    else:
        # we take each remaining point, compute its distance to the center,
        # and average those distances relative to the radius
        # the lower the returned value, the closer the remaining points are
        # to the sphere, on average.
        # instead of taking the average, we can consider taking the largest
        # distance...
        return np.average(norm(remaining_points - center, axis=1))/radius

    
def geodesic_to_euclidean(g_distances: np.ndarray, r: np.float64) -> np.ndarray:
    '''given an array of geodesic distances measured on a sphere of
    radius r, returns the corresponding euclidean distances.'''

    return r*np.sqrt(2*(1-np.cos(g_distances/r)))


class FitError(Exception):
    pass


def e_distances_to_points(e_distances: np.ndarray) -> Optional[np.ndarray]:
    '''given a matrix of euclidean distances between points on the
    3-euclidean space, returns one possible localization in the
    3-euclidean space of such points.

    - the element in position (i,j) in e_distances represents the
      euclidean distance between points i and j
    - the remaining points are then calculated using the first four
      points and their positions.
    - if any point cannot be fit, we skip it and go to the next.
    '''

    if e_distances.shape[1] < 4:
        print(f"expected e_distances to be a matrix of at least 4 columns.")
        return None

    points = np.zeros(len(e_distances)*3).reshape(len(e_distances), 3)

    # we place points[0] at the origin [0,0,0], which is already done.
    # points[1] goes to [e_distances[1,0], 0, 0]
    points[1] = np.array([e_distances[1,0], 0.0, 0.0])

    # points[2] is placed on the xy-plane, that is, z=0, and also y>0.
    # We compute it by hand:
    points[2][0] = (e_distances[2,0]**2 - e_distances[2,1]**2 \
                    + points[1,0]**2)/(2*points[1,0])
    points[2][1] = np.sqrt(e_distances[2,0]**2 - points[2][0]**2)

    # points[3] is chosen to have positive z-coordinate,
    # and is also computed by hand:
    points[3][0] = (e_distances[3,0]**2 - e_distances[3,1]**2 \
                    + points[1,0]**2)/(2*points[1,0])
    points[3][1] = (e_distances[3,1]**2 - e_distances[3,2]**2 \
                    + (points[3,0] - points[2,0])**2 \
                    - (points[3,0] - points[1,0])**2 \
                    + points[2,1]**2)/(2*points[2,1])
    points[3][2] = np.sqrt(e_distances[3,0]**2 - points[3,0]**2 - \
                           points[3,1]**2)


    for k in range(4, len(e_distances)):
        # the x and y coordinates of points[k] are computed just like we did
        # for points[3]
        points[k][0] = (e_distances[k,0]**2 - e_distances[k,1]**2 \
                        + points[1,0]**2)/(2*points[1,0])
        points[k][1] = (e_distances[k,1]**2 - e_distances[k,2]**2 \
                        + (points[k,0] - points[2,0])**2 \
                        - (points[k,0] - points[1,0])**2 \
                        + points[2,1]** 2)/(2*points[2,1])
        # the z coordinate is a bit different: we compute the 2
        # possible candidates, that differ only by sign, and take the
        # one that best matches e_distances[k,3]:
        z_sq = e_distances[k,0]**2 - points[k,0]**2 - points[k,1]**2
        if z_sq < 0:
            # something went wrong when capturing the distances
            continue
        z_candidate = np.sqrt(z_sq)

        temp_point1 = np.array([points[k,0], points[k,1], z_candidate])
        temp_point2 = np.array([points[k,0], points[k,1], -z_candidate])

        if np.abs(norm(temp_point1 - points[3]) - e_distances[k,3]) < \
           np.abs(norm(temp_point2 - points[3]) - e_distances[k,3]):
            points[k][2] = z_candidate
        else:
            points[k][2] = -z_candidate
                               
    return points


def g_distances_to_radius(g_distances: np.ndarray,
                          n_samples: int=10_000) -> np.ndarray:
    '''given a 4x4 matrix g_distances where the element (i,j) contains
    the geodesic distance between points i and j, we calculate the
    radius of the sphere that fits these 4 points. A too big value for
    the radius might mean that the points are actually coplanar (minus
    measuring errors). We try n_samples different values for the
    radius, equally spaced.

    '''

    def fit_radius(r, *args):
        '''given an initial radius r (which is a guess), we try to fit
        the given g_distances on a sphere of radius r, and then
        compare the radius of the sphere that we have actually found
        with the initial guess.

        '''
    
        e_distances = geodesic_to_euclidean(g_distances, r)
        try:
            points = e_distances_to_points(e_distances)
            r1 = find_radius(points)
            #print("if radius = {:.3f}, the error is {:.3f}%)" \
            #      .format(r, 100*np.abs((r1-r)/r)))
            return np.abs((r1-r)/r)
        except FitError:
            #print("can't fit points when r = {:.3f}".format(r))
            return np.inf

    # the minimum possible radius for a sphere contaning all the 4 points
    # is maxgd/np.pi, where maxgd is the maximum geodesic distance
    min_radius = np.max(g_distances)/np.pi

    radius = min_radius
    min_error = fit_radius(min_radius)
    for r in np.linspace(min_radius, 2*min_radius, num=n_samples):
        error = fit_radius(r)
        if error < min_error:
            min_error = error
            radius = r
        
    return radius


# next we define some geometric transformations on an array of points
def translate(points: np.ndarray, v: np.ndarray) -> np.ndarray:
    if points.shape[1] == v.shape[0]:
        return points + v
    else:
        print(f"incompatible sizes")
        return points

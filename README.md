# Round Earth

The "Round Earth" project is the lazy approach to compute the radius
of the planet Earth and also establish the position of its cities
based purely on the geodesic distances between each city and 4 chosen
"main" cities: Los Angeles, Dubai, Sydney and Tokyo.

Why these 4 cities? The original idea was to use an API of flight data
to compute the geodesic distances: if we could trust the travel
duration and average speed data from available flights, then we could
use this to compute the geodesic distances and finally reconstruct
Earth (well, at least reconstruct the position of some airports...),
given the assumption that planes fly fairly close to geodesics.

As it turns out, I didn't find a suitable free API for the task, so it
was time for plan B: there is this website [Distance
Calculator](https://www.distancecalculator.net/) where we can input
two cities and get the geodesic distance between then. On the one
hand, this defeats the purpose of the project: the geodesic distances
are calculated assuming that a geometric model for Earth is known. On
the other hand, it can still be a valid exercise to reconstruct the
planet from the geodesic distances alone.

The final ingrediente to start the process was a giant list of cities
names. That's what we have in the file 'geonames.csv'. There are files
on the Internet with more cities, but not for free. Anyway, this file
contains about 140,000 cities, more than enough to paint enough points
on the planet's surface.

## Basic usage

This is intended to be used inside the Python interpreter. We first
need a DataFrame with the 3-dimensional coordinates of the
cities. This is computed using

<code>>>> import worldbuild</code>

<code>>>> cities_points = worldbuild.earth_points_df()</code>

We are now able to create a Camera and take snapshots. A Camera is
tipically created with a target city in mind:

<code>>>> c = Camera.from_city_name('São Paulo',
                                    cities_points_df=cities_points,
				    altitude=1)</code>

The unit for the altitude parameter is the radius of Earth. We can
also pass the desired horizontal and vertical field of view of the
Camera (in degrees) using the parameters <code>hfov</code> and
<code>vfov</code>. Both these parameters default to 60 degrees.

A Camera can be translated and rotated using the
<code>translate</code> and <code>rotate</code> methods:

<code>>>> c.rotate('clockwise', 45).translate('up', 0.2)</code>

A rotation is given in degrees and a translation has the same unit as
the altitude: the radius of the planet.

Finally, we can snap a picture:

<code>>>> c.snap(file_name='name.png', visible_color='orange', invisible_color='#aabbcc')</code>

If the <code>file_name</code> is not given, we simply display the
picture. Colors can be specified in any format accepted by Matplotlib.

Since we usually build a Camera from a city's name, we can also search for cities:

<code>>>> worldbuild.search_city("New York", cities_points)<br>
21908      West New York<br>
52161      New York City<br>
98736     New York Mills<br>
118771     East New York<br>
124673    New York Mills<br>
Name: Name, dtype: object
</code>

The first parameter can be any regex:

<code>>>> worldbuild.search_city("^New York", cities_points)<br>
52161      New York City<br>
98736     New York Mills<br>
124673    New York Mills<br>
Name: Name, dtype: object
</code>


## Some details on the implementation:

The code is quite small and is divided in the following files:

* worldbuild.py: this is the module that loads the cities names and
  make the requests to the webpage to get the geodesic
  distances. Unfortunately, we can't do this using simple http
  requests, so we had to use
  [Selenium](https://pypi.org/project/selenium/) to do it and then
  fetch the desired distances by matching some regular
  expressions. This is not particularly fast, so the code supports
  doing this in batches that start many drivers to make the requests
  simultaneously. We handle the data using
  [Pandas](https://pandas.pydata.org/). Since we only needed to build
  a file with the geodesic distances once, this step is not fully
  automated, but the ingredients are there in case we want to do that.

* spherebuild.py: in this module we use the information of the
  geodesic distances to position each city in the correct place in the
  3-dimensional euclidean space. Assuming that the Earth is not only
  not flat, but round, we can find the radius that gives the best fit
  for the points from the geometric distances, and then proceed to
  position each point from the geodesic distances to the main 4
  locations. Some points cannot be adequately fit. As it turns out,
  there were a few errors when fetching the geodesic distances from
  the given webpage, but we still ended up with more than 138,000
  cities correctly identified. This module uses mainly Numpy and hand
  calculations, although there is space for a tiny use of Scipy.

* photographer.py: this modules implements the Picture and Camera
  classes. The role of the Picture class is to draw a set of 2d-points
  from their (x,y) coordinates and information about their colors and
  sizes. The picture is drawn using the scatter plot from
  [Matplotlib](https://matplotlib.org/). The Camera class is
  responsible for taking a set of points in the 3-dimensional space
  and project then in a 2d-space that represents the camera plane. We
  can define a Camera giving a city name to position it above that
  city, and we can also specify the camera altitude and field of
  view. We also implement geometric transformations to move the Camera
  around the planet.
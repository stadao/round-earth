#!/usr/bin/python3

# this module uses the 


import functools

from numbers import Number
from time import time_ns
from typing import Union, Optional

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from matplotlib.figure import Figure, Axes
from matplotlib.patches import Circle
from numpy.linalg import norm
from numpy import inner

import spherebuild
import worldbuild


deg = np.pi/180 # one degree


def time_me(f):
    """prints the elapsed time of the decorated function"""
    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        start_time = time_ns()
        val = f(*args, *kwargs)
        elapsed_time = time_ns() - start_time
        print(f"[{f.__name__}] execution time: {elapsed_time/(10.0**9):.3f} seconds")
        return val

    return wrapper
    

class Picture:
    """taking pictures of the world..."""

    def __init__(self):
        pass

    
    def __init__(self, xmin, xmax, ymin, ymax):
        """establishes the limits for our painting."""

        # we are not checking whether xmax-xmin > 0, ymax-ymin > 0...
        
        if np.abs(xmax-xmin) > np.abs(ymax-ymin):
            width = 10
            height = 10*np.abs((ymax-ymin)/(xmax-xmin))
        else:
            height = 10
            width = 10*np.abs((xmax-xmin)/(ymax-ymin))

        # we create a picture that has a maximum dimension (width or height)
        # of 10 inches
        self.figure, self.ax = plt.subplots(figsize=(width, height))        
        self.ax.axis([xmin, xmax, ymin, ymax])


    @time_me
    def add_points(self,
                   points: np.ndarray,
                   colors: np.ndarray,
                   sizes: np.ndarray,
                   zorders: np.ndarray) -> int:

        if points.shape[1] != 2:
            print((f"expected array of 2-dimensional points, "
                   "received points in dimension {points.shape[1]}"))
            return

        if colors is None:
            colors = np.full((points.shape[0],), fill_value='blue')
        if sizes is None:
            sizes = np.full((points.shape[0],), fill_value=0.2)
        if zorders is None:
            zorders = np.zeros((points.shape[0],), dtype=np.float64)

        # this following loop is quite slow...
        i = 0
        for p, c, s, z in zip(points, colors, sizes, zorders):
            i += 1
            self.ax.add_artist(Circle(p, color=c, radius=s, zorder=z))

        return i # the number of added points


    @time_me
    def quick_add_points(self,
                         points: np.ndarray,
                         colors: np.ndarray,
                         sizes: np.ndarray,
                         zorders: np.ndarray) -> int:
        """adds points to the picture:
        -> points: array of (x,y) points to add
        -> colors: array containing the color of each point
        -> sizes: array containing the size of each point
        -> zorders: its elements say whether the point is visible (1.0)
                    or invisible (-1.0)
        """
        
        # we need to break the points in two groups: first we draw the
        # invisible points, then the visible ones. The zorders array
        # contains that info.  We use pandas to build a temporary
        # DataFrame to sort the values, then convert back to numpy
        # arrays:
        points_x = points.T[0]
        points_y = points.T[1]

        aux_df = pd.DataFrame(data = {'x': points_x,
                                      'y': points_y,
                                      'colors': colors,
                                      'sizes': sizes,
                                      'zorders': zorders})

        aux_df = aux_df.sort_values('zorders', ascending=True)
        sorted_x = aux_df['x'].to_numpy(dtype=np.float64).T
        sorted_y = aux_df['y'].to_numpy(dtype=np.float64).T
        sorted_colors = aux_df['colors'].to_numpy(dtype=str).T
        sorted_sizes =  aux_df['sizes'].to_numpy(dtype=np.float64).T

        # scatter is much faster than adding each Circle as in add_points
        self.ax.scatter(sorted_x,
                        sorted_y,
                        c=sorted_colors,
                        s=sorted_sizes)
            

class Camera:

    def __init__(self,
                 position: np.ndarray,
                 cities_df: pd.DataFrame,
                 look_at: np.ndarray=np.array([1.0, 0.0, 0.0]),
                 up: np.ndarray=np.array([0.0, 0.0, 1.0]),
                 hfov: np.float64=60, # horizontal field of view, in degrees
                 vfov: np.float64=60,
                 unit: np.float64=6731): # the radius of planet Earth (km)
        """This is not meant to be called directly from c = Camera(...). Use
        c = Camera.from_city_name(...) defined below.

        -> position: the point from which we see everything. Every
           point in the 3d-space will have a line drawn to this point
           to determinet its position on the final picture. The camera
           plane is one unit from its position, that is, it passes
           through position+look_at (after normalizing look_at)        
        -> look_at: the direction that the camera is pointing. It will
           be normalized.
        -> up: the direction that points 'up' to construct what the
           camera sees. If it is not normal to look_at, we will make
           it so, and we will also normalize it.
        -> hfov and vfov: the horizontal and vertical fields of view
           of the camera, in radians. Who uses degrees, anyway?
        -> cities_df: a DataFrame with the points of the world and its
           cities.
        -> unit: we will use this as a unit of distance measure when
           translating the camera.

        """

        if position.shape != (3,):
            print(f"invalid position {position}")
            return
        if look_at.shape != (3,):
            print(f"invalid look_at {look_at}")
            return
        if norm(look_at) < 10**-9:
            print(f"look_at too small")
            return
        if hfov <= 0 or hfov >= 180:
            print(f"invalid hfov {hfov}")
            return
        if vfov <= 0 or vfov >= 180:
            print(f"invalid vfov {vfov}")
            return

        # we normalize look_at and up, and make sure they are orthonormal
        self.position = position
        self.look_at = look_at/norm(look_at)
        proj = np.inner(up, self.look_at)*self.look_at
        self.up = (up - proj)/norm(up-proj) # should we worry when up == proj?
        # self.right is the vector product (look_at x up):
        self.right = np.array([self.look_at[1]*self.up[2] - \
                               self.look_at[2]*self.up[1],
                               self.look_at[2]*self.up[0] - \
                               self.look_at[0]*self.up[2],
                               self.look_at[0]*self.up[1] - \
                               self.look_at[1]*self.up[0]])
        self.hfov = hfov*deg
        self.vfov = vfov*deg
        self.unit = unit
        self.cities_df = cities_df


    @classmethod
    def from_city_name(self,
                       city: str,
                       cities_points_df: Optional[pd.DataFrame]=None,
                       altitude: np.float64=1, 
                       hfov: np.float64=60,
                       vfov: np.float64=60):
        """this is a more natural way to define the camera: we make it
        hover over the city name provided, looking at the center of the
        world sphere.

        -> cities_points_df: DataFrame with the 'Name', 'x', 'y' and
        'z' coordinates of the world cities.

        -> altitude: if the world radius is r, we will position the
           camera at a distance equal to (1+altitude)*r from the
           origin. Hence the default value gives a camera at an
           altitude that is twice the world radius.

        -> for the default altitude, hfov and vfov should be just
           enough to capture the entire world.

        """
        if cities_points_df is None:
            cities_df = worldbuild.earth_points_df()
        else:
            cities_df = cities_points_df

        radius = spherebuild.find_radius(cities_df[0:4][['x', 'y', 'z']].to_numpy())
        city_pos = cities_df[cities_df['Name'] == city][['x', 'y', 'z']] \
                   .to_numpy()[0]
        camera_pos = (1+altitude)*radius*(city_pos/norm(city_pos))
        return Camera(camera_pos,
                      look_at = -camera_pos,
                      hfov = hfov,
                      vfov = vfov,
                      cities_df = cities_df,
                      unit = radius)


    def canvas_limits(self) -> (float, float, float, float):
        """returns the (xmin, xmax, ymin, ymax) of the points that
        will be captured in the camera plane coordinates, given the
        hfov and vfov.
        """
        xmax = np.tan(self.hfov/2.0)
        ymax = np.tan(self.vfov/2.0)

        return (-xmax, xmax, -ymax, ymax)


    def translate(self,
                  direction: str,
                  distance: float) -> None:
        """translate the camera relative to its axes self.right and
        self.up. A distance of 1 corresponds to the Earth radius."""

        if direction == "right":
            self.position += self.unit*distance*self.right
        elif direction == "left":
            self.position -= self.unit*distance*self.right
        elif direction == "up":
            self.position += self.unit*distance*self.up
        elif direction == "down":
            self.position -= self.unit*distance*self.up
        elif direction == "front":
            self.position += self.unit*distance*self.look_at
        elif direction == "back":
            self.position -= self.unit*distance*self.look_at

        return self


    def rotate(self,
               direction: str,
               angle: float) -> None:
        """rotate the camera relative to its origin, in the direction
        and angle (degrees) specified"""

        if direction == "right":
            self.right , self.look_at = np.cos(-angle*deg)*self.right + \
                                        np.sin(-angle*deg)*self.look_at, \
                                        - np.sin(-angle*deg)*self.right + \
                                        np.cos(-angle*deg)*self.look_at
        elif direction == "left":
            self.right , self.look_at = np.cos(angle*deg)*self.right + \
                                        np.sin(angle*deg)*self.look_at, \
                                        - np.sin(angle*deg)*self.right + \
                                        np.cos(angle*deg)*self.look_at            
        elif direction == "up":
            self.look_at , self.up = np.cos(angle*deg)*self.look_at + \
                                     np.sin(angle*deg)*self.up, \
                                     - np.sin(angle*deg)*self.look_at + \
                                     np.cos(angle*deg)*self.up 
        elif direction == "down":
            self.look_at , self.up = np.cos(-angle*deg)*self.look_at + \
                                     np.sin(-angle*deg)*self.up, \
                                     - np.sin(-angle*deg)*self.look_at + \
                                     np.cos(-angle*deg)*self.up
        elif direction == "counterclockwise":
            self.right , self.up = np.cos(angle*deg)*self.right + \
                                   np.sin(angle*deg)*self.up, \
                                   - np.sin(angle*deg)*self.right + \
                                   np.cos(angle*deg)*self.up
        elif direction == "clockwise":
            self.right , self.up = np.cos(-angle*deg)*self.right + \
                                   np.sin(-angle*deg)*self.up, \
                                   - np.sin(-angle*deg)*self.right + \
                                   np.cos(-angle*deg)*self.up

        return self


    def project(self, q: np.ndarray) -> Optional[np.ndarray]:
        """takes the 3d-point q and projects on the camera plane,
        which is just the plane located at distance 1 from the camera
        position and orthogonal to the camera look_at vector.
        We return the 2d coordinates relative to the base {self.right, self.up}
        of the camera plane.

        If the point is behind the camera position, we return None. If
        the point is in front of the camera, we project it even if it
        is on the same side of the camera plane as the camera
        position.
        
        In this method we don't care if the resulting point is invisible to
        the camera due to the hfov and vfov.

        """

        # first we compute the intersection of the line that goes through
        # p = self.position and q and the camera plane.
        # -> one point that belongs to the camera plane is
        #    x = self.position + self.look_at
        # -> the vector normal to the camera plane is l = self.look_at
        # -> the line that goes through p and q is (1-t)p + tq, t real.
        # we want to find t such that
        # <(1-t)p+tq - x, l> = 0. Such t is given below:
        p = self.position
        l = self.look_at
        x = p + l
        t = np.inner(l, x-p)/np.inner(l, q-p)
        y = (1-t)*p + t*q # this is still a point with 3d-coordinates.

        # we check whether q is "behind" p
        #if np.inner(q-p, y-p) <= 0:
        #    return None

        # Finally we project y to the orthonormal basis of the camera
        # plane:
        u = np.inner(y - x, self.right)
        v = np.inner(y - x, self.up)
        return np.array([u,v])


    @time_me
    def project_many(self, array_of_points: np.ndarray) -> np.ndarray:
        temp = [self.project(q) for q in array_of_points]
        # we exclude the points that were not projected.
        return np.array([p for p in temp if p is not None])


    def is_visible(self, p: np.ndarray) -> bool:
        """is the point p = (x,y,z), a city on Earth, hidden by the
        Earth and so invisible to the camera?
        """
        # q_0 is a point that belongs to the plane determined by the
        # points on Earth whose lines to self.position are tangent to
        # Earth. Also, q_0 is a multiple of self.position
        q_0 = ((self.unit**2)/norm(self.position)**2) * (self.position)
        # u is a normalized orthogonal vector to the plane that separates
        # the visible and invisible points
        u = self.position/norm(self.position)

        # this inner product is positive for the visible points
        # and negative for the invisible points
        return inner(p-q_0,u) >= 0


    def snap(self,
             visible_color='blue',
             invisible_color='0.8', # light-gray
             base_size=5,
             file_name=None):
        """with the camera info and cities_df, we snap a picture of
        the planet. If file_name is None, we call
        plt.show(). Otherwise we save the picture.

        """

        xmin, xmax, ymin, ymax = self.canvas_limits()
        scale = 100
        pic = Picture(scale*xmin, scale*xmax, scale*ymin, scale*ymax)
        points3d = self.cities_df[['x', 'y', 'z']].to_numpy()
        points2d = self.project_many(points3d)

        # we now need to build the colors, sizes and zorders arrays
        colors_aux = []
        sizes_aux = []
        zorders_aux = []
        for p in points3d:
            sizes_aux.append(self.unit*base_size/norm(p-self.position))
            if self.is_visible(p):
                colors_aux.append(visible_color)
                zorders_aux.append(1.0)
            else:
                colors_aux.append(invisible_color)
                zorders_aux.append(-1.0)

        pic.quick_add_points(scale*points2d,
                             np.array(colors_aux),
                             np.array(sizes_aux),
                             np.array(zorders_aux))

        if file_name is None:
            plt.show()
        else:
            plt.savefig(file_name, format='png')
        

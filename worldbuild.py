#!/usr/bin/python3

# In this module, we take a csv file with about 140 thousand cities
# and compute the distances between each city and 4 main locations,
# which are (IATA codes):
# -> LAX (Los Angeles)
# -> DXB (Dubai)
# -> SYD (Sydney)
# -> HND (Tokyo)

# We picked these locations because there are direct flights between
# any two of them. The original idea was to use flight data to compute
# the geodesic distances, but since the APIs available for that kind
# of information are usually paid, we activated plan B, which uses a
# website to compute the geodesic distances.
#
# This is far from ideal, since we are now using selenium to make the
# requests, which slows things down and uses a lot of memory. The
# memory problem was not addressed, but we use some multiprocessing to
# speed up the requests, since the majority of time is spent waiting
# for an answer. It is still a time consuming task...


import multiprocessing as mp
import random
import re
import numpy as np
import pandas as pd

from time import time_ns, sleep
from typing import Union, Optional

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import spherebuild


# This first part defines the functions that calculates the geodesic
# distances between each city in the csv file and the four main
# locations, which are: LAX (Los Angeles), DXB (Dubai), SYD (Sydney)
# and HND (Tokyo)
#
# The final product of this part is a sequence of files named
# 'distances_m_n.csv' which must be manually concatenated to yield
# geodesic_distances.csv (yes, that is right: I didn't write something
# to concatenate the files)
#
# Those intermediate csv files are produced by calling the batch_loop
# function


def load_webpage(driver_options: str="--headless") -> webdriver:
    """returns a driver that we can use to make the requests for
    distances"""
    
    options = Options()
    if driver_options != "":
        options.add_argument(driver_options)
    driver = webdriver.Chrome(options=options)
    driver.get("https://www.distancecalculator.net/")
    try:
        button = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, 'hae'))
            )
    except:
        print("took too long")

    return driver


def calc_distance(d: webdriver, source: str, destination: str) -> np.float64:
    """calculates the distance in km between 2 cities """

    try:
        src = d.find_element(By.ID, 'distancefrom')
        dst = d.find_element(By.ID, 'distanceto')
        src.clear()
        dst.clear()
        src.send_keys(source)
        dst.send_keys(destination)
        
        button = d.find_element(By.ID, 'hae').click()

        try:
            distance_field = WebDriverWait(d, 10, 0.2).until(
                EC.presence_of_element_located((By.ID, 'totaldistancekm'))
            )
            distance_str = distance_field.text
            # we need a regex to fetch the desired information
            result = re.search(r"([\d\.]+)\s+km", distance_str)
            return np.float64(result.group(1))
        except: #TimeOutException (selenium.common.exceptions.TimeOutException)
            print("calculation taking too long...")
            return np.float64(0.0)
    except:
        # if anything goes wrong, we reload the page and
        # return zero
        print((f"Something went wrong with {source}/{destination}."
               "Returning to defaults."))
        d.close()
        d = load_webpage()
        return np.float64(0.0)


def load_cities(file: str='geonames.csv', sep: str=',') -> pd.DataFrame:
    '''just loads a big dataframe of world cities, from which we will
    use only the city name'''
    return pd.read_csv(file, sep=sep)


def print_distances_to_main4(driver: webdriver,
                             locations: pd.DataFrame,
                             start_index: int=0,
                             n: int=10,
                             process_number: int=0) -> None:
    """returns a string where each line is of the form:
            city,toLAX,toDXB,toSYD,toHND
       containing a city name and its distances to LA, Dubai, Sydney and Tokyo.
       -> locations is just a DataFrame from which we take the cities' names
       -> the cities taken into account are those from
          locations.iloc[start_index, start_index+n]
       -> hence, this functions returns a string with n lines or less, if the
          DataFrame ends too soon.
       -> this function will be called from one of the many processes that we
          spawn to speed up the computations, so we also have info about the
          process that called it, in the parameter process_number.
    """

    if start_index >= len(locations):
        print("start_index too big.")
        return None
    
    if start_index + n >= len(locations):
        stop_index = len(locations)
    else:
        stop_index = start_index + n
    
    start = time_ns()

    lines = "" # that will be returned
    for i in range(start_index, stop_index):
        city_name = locations.iloc[i]['Name']
        toLAX = calc_distance(driver, city_name, 'Los Angeles')
        toDXB = calc_distance(driver, city_name, 'Dubai')
        toSYD = calc_distance(driver, city_name, 'Sydney')
        toHND = calc_distance(driver, city_name, 'Tokyo')
        print((f"pid: {process_number:02d}, city: {city_name[:20]:20}, "
               f"LAX: {toLAX:8.2f}, "
               f"DXB: {toDXB:8.2f}, "
               f"SYD: {toSYD:8.2f}, "
               f"HND: {toHND:8.2f}, "))
        lines += f"{city_name},{toLAX},{toDXB},{toSYD},{toHND}\n"

    stop = time_ns()
    print("pid {:2d} elapsed time: {:.2f} seconds".format(process_number,
                                                          (stop-start)/(10**9)))
    print(f"pid {process_number} results:\n{lines}")
    return lines


def batch_compute_distances(n_processes: int,
                            batch_size: int,
                            start_index: int,
                            cities_file: str="geonames.csv",
                            sep: str=";") -> None:
    """loads a DataFrame named locations with world cities and
    computes the distance of each city from

            locations.iloc[start_index, start_index + n_processes*batch_size]

    to the main 4 cities, writing a csv file with the results. In
    order to do this, we start n_processes processes to minimize the
    delay of each request to the website.
    The csv file that we write is named 'distances_firstindex_lastindex+1.csv'
    """

    locations = load_cities(cities_file, sep)
    
    def batch_job(shared_dict: dict, pid: int, batch_size: int):
        #print(f"process number {pid}")
        d = load_webpage()
        shared_dict[pid] = print_distances_to_main4(d, locations,
                                                    start_index=start_index + \
                                                                pid*batch_size,
                                                    n=batch_size,
                                                    process_number=pid)
        d.close()


    plist = []
    lines_dict = mp.Manager().dict() 
    for i in range(n_processes):
        plist.append(mp.Process(target=batch_job, args=(lines_dict, i, batch_size)))

    start = time_ns()
    for p in plist:
        p.start()

    for p in plist:
        p.join()

    # return lines_dict # we can now use lines_dict.values() to write a csv file
    filename = f"distances_{start_index}_{start_index+n_processes*batch_size}.csv"
    
    with open(filename, "w") as f:
        for l in lines_dict.values():
            f.write(l)

    stop = time_ns()
    print("written {:d} lines in {:.2f} seconds".format(n_processes*batch_size,
                                                        (stop-start)/(10**9)))


def batch_loop(n_iter: Optional[int],
               n_processes: int,
               batch_size: int,
               start_index: int,
               cities_file: str="geonames.csv",
               sep: str=";") -> None:

    """calls batch_compute_distances n_iter times..."""

    i = 0
    while True:
        if (n_iter is not None) and (i+1 > n_iter):
            return None
        
        print((f"starting iteration {i+1} of {n_iter}: "
               f"start_index={start_index + i*n_processes*batch_size}"))
        batch_compute_distances(n_processes,
                                batch_size,
                                start_index + i*n_processes*batch_size,
                                cities_file=cities_file,
                                sep=sep)
        i += 1


# In this second part, we already have a csv file with all the
# geodesic distances computed from each city to the main 4 locations.
# We have also computed the geodesic distances between the 4 main
# cities separately in the file 'main4cities.csv'

def clean_distances_df(distances: pd.DataFrame) -> pd.DataFrame:
    """given a DataFrame with geodesic distances to the main 4 cities,
    we drop the rows in which all the computed distances are zero.

    """

    df = distances.copy()

    df['sum'] = df['toLAX'] + df['toDXB'] + df['toSYD'] + df['toHND']
    # we convert sum to NA  if it is zero, so we can call pd.dropna
    df['sum'] = df.apply(lambda x: pd.NA if x['sum'] == 0.0 else x['sum'],
                         axis=1)

    answer = df.dropna(axis=0)
    del answer['sum'] # not needed anymore
    return answer


def all_geodesic_distances_df(main4file = 'main4cities.csv',
                              othersfile = 'geodesic_distances.csv') -> pd.DataFrame:
    """returns a DataFrame with the geodesic distances of every city
    to the main 4 locations"""

    main4_df = pd.read_csv(main4file)
    others_df = pd.read_csv(othersfile,
                            names=['Name', 'toLAX', 'toDXB', 'toSYD', 'toHND'])

    return pd.concat([main4_df, clean_distances_df(others_df)],
                     ignore_index=True)


def earth_points_df(main4file = 'main4cities.csv',
                    othersfile = 'geodesic_distances.csv') -> pd.DataFrame:
    """returns a DataFrame with the name and the possible (x,y,z)
    coordinates of every city. The earth sphere has center (0,0,0).
    """
    
    earth_geodesic_df = all_geodesic_distances_df(main4file, othersfile)
    earth_geodesic_np = earth_geodesic_df[['toLAX', 'toDXB', 'toSYD', 'toHND']] \
                        .to_numpy()
    earth_radius = spherebuild.g_distances_to_radius(earth_geodesic_np[0:4])
    earth_euclidean = spherebuild.geodesic_to_euclidean(earth_geodesic_np,
                                                        earth_radius)
    earth_points = spherebuild.e_distances_to_points(earth_euclidean)
    # we now move the center of the earth sphere to the origin
    earth_center = spherebuild.find_center(earth_points[0:4])
    earth_points = spherebuild.translate(earth_points, -earth_center)

    earth_points_df = pd.DataFrame(earth_points, columns=['x', 'y', 'z'])

    # we add dropna() to the returned DataFrame because some (few)
    # locations returned inconsistent values from the web requests,
    # which made it impossible to locate them on a sphere
    return earth_geodesic_df[['Name']].merge(earth_points_df,
                                             left_index=True,
                                             right_index=True) \
                                      .dropna()


def search_city(name: str, earth_points_df: pd.DataFrame) -> pd.DataFrame:
    """prints all the city names in earth_points_df that match 'name'.
    -> name: string or regex to search
    -> earth_points_df: DataFrame that contains a field 'Name'
    """

    result = earth_points_df['Name'].apply(lambda x: re.search(name, x)) \
                                    .dropna()
    result = result.apply(lambda x: x.string)
    return result
